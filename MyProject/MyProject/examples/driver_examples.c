/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file
 * to avoid losing it when reconfiguring.
 */

#include "driver_examples.h"
#include "driver_init.h"
#include "utils.h"
#include "RGBdriver.h"

static uint8_t rgbColor;
static struct timer_task TIMER_0_task1, TIMER_0_task2;


static void button_on_PA09_pressed(void)
{
	if (gpio_get_pin_level(PIN_PA09)==false){
		RGB_setColor(rgbColor);
		rgbColor += 1;
		rgbColor = rgbColor % 7;
		
		timer_start(&TIMER_0);
	}
	else{
		TIMER_0.time=0;
		timer_stop(&TIMER_0);
	}
	
}

/** 
 * Example of using EXTERNAL_IRQ_0
 */
void EXTERNAL_IRQ_0_example(void)
{

	ext_irq_register(PIN_PA09, button_on_PA09_pressed);
}


/**
 * Example of using TIMER_0.
 */
static void TIMER_0_task1_cb(const struct timer_task *const timer_task)
{
	gpio_toggle_pin_level(LED_VERDE);
}

static void TIMER_0_task2_cb(const struct timer_task *const timer_task)
{
}

void TIMER_0_example(void)
{
	TIMER_0_task1.interval = 3;
	TIMER_0_task1.cb       = TIMER_0_task1_cb;
	TIMER_0_task1.mode     = TIMER_TASK_REPEAT;
	TIMER_0_task2.interval = 200;
	TIMER_0_task2.cb       = TIMER_0_task2_cb;
	TIMER_0_task2.mode     = TIMER_TASK_REPEAT;

	timer_add_task(&TIMER_0, &TIMER_0_task1);
	timer_add_task(&TIMER_0, &TIMER_0_task2);
	timer_start(&TIMER_0);
}
