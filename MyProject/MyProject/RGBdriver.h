
#ifndef RGBDRIVER_H_
#define RGBDRIVER_H_

#define RGB_RED			0x0
#define RGB_YELLOW		0x1
#define RGB_GREEN		0x2
#define RGB_BLUE		0x3
#define RGB_PURPLE		0x4
#define RGB_WHITE		0x5


#include <atmel_start.h>


void RGB_setColor(uint8_t color);


#endif /* RGBDRIVER_H_ */