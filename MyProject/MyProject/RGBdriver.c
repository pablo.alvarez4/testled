#include "RGBdriver.h"

void RGB_setColor(uint8_t color)
{
	switch (color)
	{
		case RGB_RED:
		{
			hri_tccount8_write_CC_reg(TC0, 0, 0xff);
			hri_tccount8_write_CC_reg(TC0, 1, 0x00);
			hri_tccount8_write_CC_reg(TC1, 0, 0x00);
			break;
		}	
		case RGB_YELLOW:
		{
			hri_tccount8_write_CC_reg(TC0, 0, 0xff);
			hri_tccount8_write_CC_reg(TC0, 1, 0xff);
			hri_tccount8_write_CC_reg(TC1, 0, 0x00);
			break;
		}
		case RGB_GREEN:
		{
			hri_tccount8_write_CC_reg(TC0, 0, 0x00);
			hri_tccount8_write_CC_reg(TC0, 1, 0xff);
			hri_tccount8_write_CC_reg(TC1, 0, 0x00);
			break;
		}
		case RGB_BLUE:
		{
			hri_tccount8_write_CC_reg(TC0, 0, 0x00);
			hri_tccount8_write_CC_reg(TC0, 1, 0x00);
			hri_tccount8_write_CC_reg(TC1, 0, 0xff);
			break;
		}
		case RGB_PURPLE:
		{
			hri_tccount8_write_CC_reg(TC0, 0, 0xff);
			hri_tccount8_write_CC_reg(TC0, 1, 0x00);
			hri_tccount8_write_CC_reg(TC1, 0, 0xff);
			break;
		}
		case RGB_WHITE:
		{
			hri_tccount8_write_CC_reg(TC0, 0, 0xff);
			hri_tccount8_write_CC_reg(TC0, 1, 0xff);
			hri_tccount8_write_CC_reg(TC1, 0, 0xff);
			break;
		}
		default:
		{	
			hri_tccount8_write_CC_reg(TC0, 0, 0x00);
			hri_tccount8_write_CC_reg(TC0, 1, 0x00);
			hri_tccount8_write_CC_reg(TC1, 0, 0x00);
		}
	}
}
